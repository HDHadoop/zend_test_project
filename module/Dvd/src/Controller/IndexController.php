<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Dvd\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Album\Model\DvdTable;

class IndexController extends AbstractActionController
{
	protected $table;

	public function __construct($table) {
		$this->table = $table;
	}

    public function indexAction()
    {
    	$dvds = $this->table->fetchAll();

    	foreach($dvds as $dvd) {
    		echo 'title: ' . $dvd->getTitle() . ' plot: ' . $dvd->getPlot() . '<br/>';
    	}

        return new ViewModel();
    }
}
