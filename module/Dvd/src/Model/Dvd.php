<?php

namespace Dvd\Model;

class Dvd {
	protected $id;
	protected $title;
	// protected $plot;
	// protected $director;
	// protected $image;
	// protected $rating;

	public function exchangeArray(array $data) {
		$this->id = $data['id'];
		$this->title = $data['title'];
		$this->plot = $data['plot'];
		// $this->director = $data['director'];
		// $this->image = $data['image'];
		// $this->rating = $data['rating'];
	}

	public function getTitle() {
		return $this->title;
	}

	public function getPlot() {
		return $this->plot;
	}
}