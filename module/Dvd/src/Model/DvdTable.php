<?php

namespace Dvd\Model;

use Zend\Db\TableGateway\TableGatewayInterface;

class DvdTable {
	protected $tableGateway;

	public function __construct(TableGatewayInterface $tableGateway) {
		$this->tableGateway = $tableGateway;
	}

	public function fetchAll()
    {
        return $this->tableGateway->select();
    }
}